//
//  ViewController.m
//  rutrackerClient
//
//  Created by almakaev iliyas on 02.07.15.
//  Copyright (c) 2015 almakaev iliyas. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"

#define kURLString                  @"http://login.rutracker.org/forum/login.php"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    AFSecurityPolicy *policy = [[AFSecurityPolicy alloc] init];
    [policy setAllowInvalidCertificates:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setSecurityPolicy:policy];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:kURLString parameters:[NSDictionary dictionary] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *htmlData = [[NSString alloc] initWithData:responseObject encoding:NSWindowsCP1251StringEncoding];
        [self loginWithName:@"hityr" password:@"12345" prevHTMLData:htmlData withBlock:^(NSString *resultString) {
            NSLog(@"result:%@", resultString);
        }];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error:%@", [error localizedDescription]);
    }];
}

- (void)loginWithName:(NSString *)login password:(NSString *)password prevHTMLData:(NSString *)htmlData withBlock:(void(^)(NSString *resultString))block {
    NSString *loginName = [self searchExpressionWithPattern:@"login\\d{4}" inString:htmlData];
    NSString *passwsName = [self searchExpressionWithPattern:@"passwd\\d{4}" inString:htmlData];
    if (loginName == nil || password == nil) {
        block(nil);
        return;
    }
    AFSecurityPolicy *policy = [[AFSecurityPolicy alloc] init];
    [policy setAllowInvalidCertificates:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager setSecurityPolicy:policy];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSDictionary *parameters = @{loginName:login, passwsName:password};
    [manager POST:kURLString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *htmlData = [[NSString alloc] initWithData:responseObject encoding:NSWindowsCP1251StringEncoding];
        block(htmlData);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error:%@", [error localizedDescription]);
        block(nil);
    }];
}

- (NSString *)searchExpressionWithPattern:(NSString *)pattern inString:(NSString *)dataSource {
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    NSTextCheckingResult *checkingResult = [regex firstMatchInString:dataSource options:NSMatchingReportProgress range:NSMakeRange(0, dataSource.length)];
    NSString *result = [dataSource substringWithRange:checkingResult.range];
    return result;
}

@end
